package com.zhenwo.zlb.sso;

/**
 *  对应Sso的业务错误
 */
public class SsoError extends RuntimeException {

    public SsoError(String errmsg) {
        super(errmsg);
    }

}
