package com.zhenwo.zlb.sso;

import lombok.Data;

@Data
public class SsoResult {
    private String result;
    private String errmsg;

    public boolean IsError() {
        return !result.equals("0");
    }

    public SsoError getError() {
        return new SsoError("["+result+"]"+errmsg);
    }
}
