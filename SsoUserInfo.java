package com.zhenwo.zlb.sso;

/**
 * sso 用户信息对象
 *
 * @author chenk.zhenwo
 */
@lombok.Data
public class SsoUserInfo extends SsoResult {
    private String userid;
    private String authlevel;
    private String username;
    private String idnum;
    private String sex;
    private String nation;
    private String loginname;
    private String email;
    private String mobile;
    private String postcode;
    private String cakey;
    private String birthday;
    private String country;
    private String province;
    private String city;
    private String officeaddress;
    private String officephone;
    private String officefax;
    private String homephone;
    private String homeaddress;
    private String useable;
    private String orderby;
    private String headpicture;
}
