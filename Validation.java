package com.zhenwo.zlb.sso;

@lombok.Data
public class Validation extends SsoResult {

    private String token;
    private String userid;
    private String loginname;
    private String username;

}
