# 浙里办SSO对接Java模块

本模块是浙里办APP登录SSO的对接Java模块。来源于实际对接项目，有需要的可以直接拉取使用。

API涉及两个服务器的两组服务，API说明见：

[浙里办SSO API(1)](https://rongapi.cn/api/detail/40)

[浙里办SSO API(2)](https://rongapi.cn/api/detail/41)

## 调用方法

前台获取到Token传输到后台之后，直接调用

```java
sso.Login(token)
```

认证过程包含token鉴权和调用用户信息两个步骤，首先调用验证接口验证token的有效性，然后调取getUserInfoByI获取用户信息。

### 返回类型

SsoUserInfo

| userid        | 用户在SSO分配的身份唯一号                                    |
| ------------- | ------------------------------------------------------------ |
| authlevel     | 认证级别1.匿名2.实名3.实人（当authlevel认证级别为2或者3的时候实名信息才有String效） |
| username      | 实名信息--用户真实姓名                                       |
| idnum         | 实名信息--证件号码（身份证）                                 |
| sex           | 实名信息--性别1男2女                                         |
| nation        | 实名信息--民族(见国标GB3304-91)                              |
| loginname     | 登录名                                                       |
| email         | 邮件                                                         |
| mobile        | 手机号码                                                     |
| postcode      | 邮编                                                         |
| cakey         | CA证书KEY                                                    |
| birthday      | 生日                                                         |
| country       | 国籍                                                         |
| province      | 省籍                                                         |
| city          | 城市                                                         |
| officeaddress | 办公地址                                                     |
| officephone   | 办公电话                                                     |
| officefax     | 办公传真                                                     |
| homephone     | 家庭电话                                                     |
| homeaddress   | 家庭地址                                                     |
| useable       | 用户激活状态1激活，2未激活                                   |
| orderby       | 排序                                                         |
| headpicture   | 头像地址：http://[sso4]/[headpicture]注意：该地址为外网能访问的地址 |

### 返回错误

#### 1、认证错误 SsoError

认证错误时，会抛出SsoError错误，错误信息格式为"[code]msg"，括号内为错误代号，后面为错误信息

#### 2、HTTPS错误 KeyManagementException, NoSuchAlgorithmException

和HTTPS相关的错误，如果环境没有问题，一般不会出

#### 3、URL配置错误 URISyntaxException

如果修改了配置，可能会出该错误。

## 推荐调用方式

```java
try {
	SsoUserInfo ssoUserInfo = Sso.Login(token);
	//处理登录成功逻辑
}catch(SsoError err){
	//处理登录逻辑错误，登录信息问题等，可认为是业务逻辑错误，推荐业务逻辑处理
}catch(Exception e){
	//处理其他错误，基本可认为是内部逻辑错误，需要开发验证修正
}
```

